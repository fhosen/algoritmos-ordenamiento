#!/usr/bin/env python
#encoding: utf-8

# Ordena el arreglo in-place.
def selection_sort(arr):
    # Invariante: arr[0, i] está ordenado y son los mínimos.
    for i in xrange(0, len(arr)):
        # busco el próximo elemento más chico y lo ubico adelante.
        i_min = min_index_from(i, arr)
        arr[i], arr[i_min] = arr[i_min], arr[i]

def insertion_sort(arr):
    # Invariante: arr[0, i] está ordenado.
    for i in xrange(0, len(arr)):
        # Inserto el próximo elemento de forma ordenada.
        j, k = i-1, i
        while (j >= 0 and arr[j] > arr[k]):
            a[j], a[k] = a[k], a[j]
            k, j = j, j-1

def bubble_sort(arr):
    must_check_array = True
    # Hasta que no haya swaps tengo que revisar el arreglo.
    while (must_check_array):
        must_check_array = False
        for i in xrange(0, len(arr) - 1):
            if arr[i] > arr[i+1]:
                arr[i], arr[i+1] = arr[i+1], arr[i]
                must_check_array = True


# Funciones Auxiliares
# Devuelve el índice del primer elemento más chico de arr[from_i, len(arr)].
# PRE: 0 <= from_i < len(arr)
def min_index_from(from_i, arr):
    min_i = from_i
    for j in xrange(from_i, len(arr)):
        if (arr[j] < arr[min_i]):
            min_i = j

    return min_i
